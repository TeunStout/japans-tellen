package japans.Repository;

import japans.Entity.Counting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CountingRepository implements CountingRepositoryI {

    @Autowired
    EntityManager em;

    @Override
    public Counting getCoutingById(long number) {
        return em.find(Counting.class, number);
    }

    @Override
    public void SaveOneCounting(Counting data) {
        em.persist(data);
    }

    @Override
    public void SaveCounting(List<Counting> data) {
        for (Counting counting : data) {
            em.persist(counting);
        }
    }

    @Override
    public Counting deleteCounting(long number) {
        Counting delCount = em.find(Counting.class, number);
        em.remove(delCount);
        return delCount;
    }
}
