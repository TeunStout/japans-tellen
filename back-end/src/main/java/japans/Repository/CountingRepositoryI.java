package japans.Repository;

import japans.Entity.Counting;

import java.util.List;

public interface CountingRepositoryI {

    public Counting getCoutingById(long number);
    public void SaveOneCounting(Counting data);
    public void SaveCounting(List<Counting> data);
    public Counting deleteCounting(long number);

}
