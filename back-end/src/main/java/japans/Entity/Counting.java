package japans.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Counting {

    @Id
    private long number;
    @NotNull
    private String englishNumber;
    private String japaneseNumber;

    protected Counting(){

    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getEnglishNumber() {
        return englishNumber;
    }

    public void setEnglishNumber(String englishNumber) {
        this.englishNumber = englishNumber;
    }

    public String getJapaneseNumber() {
        return japaneseNumber;
    }

    public void setJapaneseNumber(String japaneseNumber) {
        this.japaneseNumber = japaneseNumber;
    }
}
